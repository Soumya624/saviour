import React from 'react'
import { Link } from 'react-router-dom';
import {
    Card, Button, CardImg, CardTitle, CardText, CardColumns,
    CardSubtitle, CardBody, Row, Col, Modal, Form, Nav, InputGroup, Navbar, FormControl, NavDropdown
  } from 'react-bootstrap';
import { BsSearch, BsFillGearFill, BsFillBellFill } from "react-icons/bs";

function NewsnavComponent() {
    return (
        <div>
        {/* <Navbar className="bg-light justify-content-between">
        <Form inline>
            <div style={{display: "flex", justifyContent: "space-between"}}>
            <BsSearch style={{marginTop:"10px", marginRight:"2px"}}/>
            <InputGroup style={{marginLeft:"8px"}}>
            <FormControl
                placeholder="Search"
                aria-label="Username"
                aria-describedby="basic-addon1"
            />
            </InputGroup>
            <Link to="/Donor_Feed">Feeds</Link>
            <Link to="/Donor_Dashboard">Dashboard</Link>
            <Link to="/Donor_News">News</Link>
            </div>
        </Form>
        <div>
            
        </div>
        <div>
        <BsFillBellFill style={{margin:"0 10px"}}/>
        <BsFillGearFill/>
        </div>
        </Navbar>  */}
        <Navbar bg="light" expand="lg">
        <div style={{display: "flex", justifyContent: "space-between"}}>
        <BsSearch style={{marginTop:"10px", marginRight:"10px"}}/>
        <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
        </Form>
        </div>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/Student_Dashboard">Dashboard</Nav.Link>
            <Nav.Link href="/Student_News">News</Nav.Link>
            </Nav>
            <div>
            <BsFillBellFill style={{margin:"0 10px"}}/>
            <BsFillGearFill/>
            </div>
        </Navbar.Collapse>
        </Navbar>
        </div>
    )
}

export default NewsnavComponent
