import React, {useState} from 'react';
import {
  Card, Button, CardImg, CardTitle, CardText, CardColumns,
  CardSubtitle, CardBody, Row, Col, Modal, Form
} from 'react-bootstrap';
const Example = (props) => {

  const [show, setShow] = useState(false);
  const [show1, setShow1] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handle1Close = () => setShow1(false);
  const handle1Show = () => setShow1(true);

  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);
  const handle2Close = () => setShow2(false);
  const handle2Show = () => setShow2(true);
  const handle3Close = () => setShow3(false);
  const handle3Show = () => setShow3(true);

  return (
    <div>
    <Row style={{padding: "9% 5%"}}>
        <Col style={{margin: "8% 0%"}}>
            <h1>Welcome to Saviour. Press the Following Buttons to Get Started!</h1>
            <br/><br/>
            <Button variant="outline-dark" onClick={handleShow}>Donor Login</Button>{' '}
            <Button variant="outline-dark" onClick={handle1Show}>Student Login</Button>{' '}
        </Col>
        <Col>
          <img src="./world.png" />
        </Col>
    </Row>

    {/* Donor Login */}
     <Modal
     show={show}
     onHide={handleClose}
     backdrop="static"
     keyboard={false}
   >
     <Modal.Body style={{padding:"10%"}}>
     <center><h4>Welcome Donor!</h4></center>
     <Form>
        <Form.Group controlId="formBasicEmail">
            <Form.Label>Username</Form.Label>
            <Form.Control type="email" placeholder="Enter Your Username" />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Enter Your Password" />
        </Form.Group>
        <center>
        <p><Button variant="link" style={{textDecoration:"none"}} onClick={function(event){ handleClose(); handle2Show()}}>Don't Have an Account? Get Started</Button></p>
        <Button variant="outline-dark" type="submit" style={{margin:"1%"}}>
            Signin
        </Button>
        <Button variant="outline-dark" type="submit" onClick={handleClose} style={{margin:"1%"}}>
            Cancel
        </Button>
        </center>
    </Form>
     </Modal.Body>
   </Modal>

    {/* Student Login */}
   <Modal
     show={show1}
     onHide={handle1Close}
     backdrop="static"
     keyboard={false}
   >
     <Modal.Body style={{padding:"10%"}}>
     <center><h4>Welcome Student!</h4></center>
     <Form>
        <Form.Group controlId="formBasicEmail">
            <Form.Label>Username</Form.Label>
            <Form.Control type="email" placeholder="Enter Your Username" />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Enter Your Password" />
        </Form.Group>
        <center>
        <p><Button variant="link" style={{textDecoration:"none"}} onClick={function(event){ handle1Close(); handle3Show()}}>Don't Have an Account? Get Started</Button></p>
        <Button variant="outline-dark" type="submit" style={{margin:"1%"}}>
            Signin
        </Button>
        <Button variant="outline-dark" type="submit" onClick={handle1Close} style={{margin:"1%"}}>
            Cancel
        </Button>
        </center>
    </Form>
     </Modal.Body>
   </Modal>

    {/* Donor Signup */}
   <Modal
     show={show2}
     onHide={handle2Close}
     backdrop="static"
     keyboard={false}
   >
     <Modal.Body style={{padding:"10%"}}>
     <center><h4>Welcome Donor!</h4></center>
     <Form>
        <Form.Group controlId="formBasicEmail">
            <Form.Label>Email ID</Form.Label>
            <Form.Control type="email" placeholder="Enter Your Username" />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Enter Your Password" />
        </Form.Group>
        <Form.Group controlId="formBasicPassword1">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control type="password" placeholder="Re-Enter Your Password" />
        </Form.Group>
        <center>
        <p><Button variant="link" style={{textDecoration:"none"}} onClick={function(event){ handle2Close(); handleShow()}}>Already Have an Account? Login Here</Button></p>
        <Button variant="outline-dark" type="submit" style={{margin:"1%"}}>
            Signup
        </Button>
        <Button variant="outline-dark" type="submit" onClick={handle2Close} style={{margin:"1%"}}>
            Cancel
        </Button>
        </center>
    </Form>
     </Modal.Body>
   </Modal>

    {/* Student Signup */}
   <Modal
     show={show3}
     onHide={handle3Close}
     backdrop="static"
     keyboard={false}
   >
     <Modal.Body style={{padding:"10%"}}>
     <center><h4>Welcome Student!</h4></center>
     <Form>
        <Form.Group controlId="formBasicEmail">
            <Form.Label>Email ID</Form.Label>
            <Form.Control type="email" placeholder="Enter Your Username" />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Enter Your Password" />
        </Form.Group>
        <Form.Group controlId="formBasicPassword1">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control type="password" placeholder="Re-Enter Your Password" />
        </Form.Group>
        <center>
        <p><Button variant="link" style={{textDecoration:"none"}} onClick={function(event){ handle3Close(); handle1Show()}}>Already Have an Account? Login Here</Button></p>
        <Button variant="outline-dark" type="submit" style={{margin:"1%"}}>
            Signup
        </Button>
        <Button variant="outline-dark" type="submit" onClick={handle3Close} style={{margin:"1%"}}>
            Cancel
        </Button>
        </center>
    </Form>
     </Modal.Body>
   </Modal>


   </div>
  );
};

export default Example;