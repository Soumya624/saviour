import React from 'react'
import {
    Card, Button, CardImg, CardTitle, CardText, CardColumns,
    CardSubtitle, CardBody, Row, Col, Modal, Form, Pagination
  } from 'react-bootstrap';
import Dashcard from './DashcardComponent'
import Newstopnav from './NewstopnavComponent'

export default function NewsComponent() {
    return (
        <div>
            <Newstopnav/>
            <div style={{padding:"0 4%"}}>
                <Dashcard/>
                <Dashcard/>
            </div>
        </div>
    )
}
