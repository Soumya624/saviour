import React from 'react'
import {
    Card, Button, CardImg, CardTitle, CardText, CardColumns,
    CardSubtitle, CardBody, Row, Col, Modal, Form, Pagination
  } from 'react-bootstrap';
import Newscard from './NewscardComponent'
import Newsnav from './NewsnavComponent'
import Newstopnav from './NewstopnavComponent'

export default function NewsComponent() {
    return (
        <div>
            <Newstopnav/>
            <Newsnav/>
            <div style={{padding:"0 4%"}}>
                <Newscard/>
                <Newscard/>
                <Newscard/>
                <Newscard/>
            </div>
        </div>
    )
}
