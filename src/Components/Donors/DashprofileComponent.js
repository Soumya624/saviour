import React from 'react'
import {
    Card, Button, CardImg, CardTitle, CardText, CardColumns,
    CardSubtitle, CardBody, Row, Col, Modal, Form, Pagination, FormControl, InputGroup
  } from 'react-bootstrap';
import Newstopnav from './NewstopnavComponent'
import { BsServer, BsFillPersonFill, BsChevronRight } from "react-icons/bs";
import logo from '../../logo.svg';
export default function NewsComponent() {
    return (
        <div>
            <Newstopnav/>
            <div style={{padding:"0 4%"}}>
            <Card style={{margin: "2% 0%", backgroundColor:"#fafbfc"}}>
                <Card.Body>
                    <Row style={{alignItems:"center"}}>
                        <Col xs={9}>
                            <Card.Text variant="center" style={{textAlign:"left"}}>
                            <h4>Academic Details</h4>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </Card.Text>
                        </Col>
                        <Col xs={3}>
                            <Card.Img variant="top" src="acad1.png" style={{width:"70%"}}/>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
            <Card style={{margin: "2% 0%", backgroundColor:"#fafbfc"}}>
                <Card.Body>
                    <Row style={{alignItems:"center"}}>
                        <Col xs={4}>
                            <h4>Journal</h4>
                        </Col>
                        <Col xs={8}>
                            <Card.Text variant="center" style={{textAlign:"left"}}>
                                Date: xx-xx-xxxx
                            </Card.Text>
                        </Col>
                    </Row><br/>
                    <Row style={{alignItems:"center"}}>
                        <Col xs={3}>
                            
                        </Col>
                        <Col xs={9}>
                            <Card.Text variant="center" style={{textAlign:"left"}}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </Card.Text>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
            <Card style={{margin: "2% 0%", border:"none"}}>
                <Card.Body>
                    <Row style={{alignItems:"center"}}>
                        <Col xs={3}>
                            <Card.Img variant="top" src={logo} />
                        </Col>
                        <Col xs={9}>
                            <Card.Text variant="center" style={{textAlign:"left"}}>
                            Lorem Ipsum<br/><br/>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </Card.Text>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>       
            </div>
        </div>
    )
}
