import React from 'react'
import { Link } from 'react-router-dom';
import {
    Card, Button, CardImg, CardTitle, CardText, CardColumns,
    CardSubtitle, CardBody, Row, Col, Modal, Form
  } from 'react-bootstrap';
import { BsChevronRight } from "react-icons/bs";
import logo from '../../logo.svg';
export default function NewsComponent() {
    return (
        <div>
            <Card style={{margin: "2% 0%", backgroundColor:"#fafbfc"}}>
                <Card.Body>
                    <Row style={{alignItems:"center"}}>
                        <Col xs={3}>
                            <Card.Img variant="top" src={logo} />
                        </Col>
                        <Col xs={8}>
                            <Card.Text variant="center" style={{textAlign:"left"}}>
                            Lorem Ipsum<br/><br/>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </Card.Text>
                        </Col>
                        <Col xs={1}>
                            <Link to='/Individual_Profile'><BsChevronRight/></Link>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>        
        </div>
    )
}
