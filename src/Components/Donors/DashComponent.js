import React from 'react'
import { Link } from 'react-router-dom';
import {
    Card, Button, CardImg, CardTitle, CardText, CardColumns,
    CardSubtitle, CardBody, Row, Col, Modal, Form, Pagination
  } from 'react-bootstrap';
import Newstopnav from './NewstopnavComponent'
import { BsServer, BsFillPersonFill } from "react-icons/bs";
import logo from '../../logo.svg';
export default function NewsComponent() {
    return (
        <div>
            <Newstopnav/>
            <div style={{padding:"0 4%"}}>
            <Card style={{margin: "2% 0%", backgroundColor:"#fafbfc", border:"none"}}>
                <Card.Body>
                    <Row style={{alignItems:"center"}}>
                        <Col xs={6}>
                            <img src="https://www.freeiconspng.com/thumbs/person-icon/clipart--person-icon--cliparts-15.png" alt="" style={{width:"6rem"}}/>
                            <br/><Link to="/Donor_Count" style={{textDecoration:"none", color:"black"}}>2</Link>
                        </Col>
                        <Col xs={6}>
                            <img src="logo2.png" alt="" style={{width:"8rem"}}/>
                            <br/><Link to="" style={{textDecoration:"none", color:"black"}}>1025</Link>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
            <Card style={{margin: "2% 0%"}}>
                <Card.Body>
                    <Row style={{alignItems:"center"}}>
                        <Col xs={12}>
                            <Card.Text variant="center" style={{textAlign:"left"}}>
                            Lorem Ipsum<br/><br/>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat<br/><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                            </Card.Text>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
            </div>
        </div>
    )
}
