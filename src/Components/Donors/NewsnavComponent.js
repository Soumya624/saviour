import React from 'react'
import {
    Card, Button, CardImg, CardTitle, CardText, CardColumns,
    CardSubtitle, CardBody, Row, Col, Modal, Form, Nav
  } from 'react-bootstrap';
  
function NewsnavComponent() {
    return (
        <div>
        <Nav className="justify-content-center" activeKey="/home">
            <Nav.Item>
            <Nav.Link eventKey="link-0">Link</Nav.Link>
            </Nav.Item>
            <Nav.Item>
            <Nav.Link eventKey="link-1">Link</Nav.Link>
            </Nav.Item>
            <Nav.Item>
            <Nav.Link eventKey="link-2">Link</Nav.Link>
            </Nav.Item>
            <Nav.Item>
            <Nav.Link eventKey="link-3">Link</Nav.Link>
            </Nav.Item>
        </Nav>   
        </div>
    )
}

export default NewsnavComponent
