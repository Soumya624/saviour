import logo from './logo.svg';
import './App.css';
import Reg from './Components/RegistrationComponent'

import DoNews from './Components/Donors/NewsComponent';
import DoFeed from './Components/Donors/FeedComponent';
import DoCount from './Components/Donors/DashcountComponent';
import DoDash from './Components/Donors/DashComponent';
import DoInd from './Components/Donors/DashprofileComponent';

import StNews from './Components/Students/NewsComponent';
import StDash from './Components/Students/DashComponent';
import StPar from './Components/Students/ParentsComponent';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
function App() {
  return (
    <div className="App">
      <Router>
        <Route  path='/' component={Reg} exact />
        <Route path='/Donor_News' component={DoNews} />
        <Route path='/Donor_Feed' component={DoFeed} />
        <Route path='/Donor_Count' component={DoCount} />
        <Route path='/Donor_Dashboard' component={DoDash} />
        <Route path='/Individual_Profile' component={DoInd} />
        <Route path='/Student_News' component={StNews} />
        <Route path='/Student_Dashboard' component={StDash} />
        <Route path='/Student_Parents' component={StPar} />
      </Router>
    </div>
  );
}

export default App;
